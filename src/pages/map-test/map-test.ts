import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {
  GoogleMap,
  GoogleMapsEvent
} from '@ionic-native/google-maps';

declare var google: any;

@Component({
  selector: 'page-map-test',
  templateUrl: 'map-test.html',
})
export class MapTestPage {
  @ViewChild('map') mapRef: ElementRef
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapTestPage');
  }
}
