import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {HomePage} from '../home/home';
import {TabBusinessPage} from '../tab-business/tab-business';
import {TabEntertainmentPage} from '../tab-entertainment/tab-entertainment';
import {TabGeneralPage} from '../tab-general/tab-general';
import {TabHealthPage} from '../tab-health/tab-health';
import {TabSciencePage} from '../tab-science/tab-science';
import {TabSportsPage} from '../tab-sports/tab-sports';
import {UserProvider} from '../../service/GlobalVar'



@Component({
  selector: 'page-test1',
  templateUrl: 'test1.html',
})
export class Test1Page {

  tabBusinessRoot: any = TabBusinessPage;
  tabEntertainmentRoot: any = TabEntertainmentPage;
  tabGeneralRoot: any = TabGeneralPage;
  tabHealthRoot: any = TabHealthPage;
  tabScienceRoot: any = TabSciencePage;
  tabSportsRoot: any = TabSportsPage;
  ct = 'us';
  currentTab: number;
  countries =[
    {name:'us'},{name:'ae'},{name:'ar'},{name:'at'},{name:'au'},{name:'be'},{name:'bg'},{name:'kr'},{name:'jp'},
    {name:'br'},{name:'ca'},{name:'ch'},{name:'cn'},{name:'co'},{name:'cu'},{name:'cz'},{name:'de'},{name:'eg'},
    {name:'fr'},{name:'gb'},{name:'gr'},{name:'hk'},{name:'hu'},{name:'id'},{name:'ie'},{name:'il'},{name:'in'},{name:'ve'},
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider) {
  }
  ionViewDidEnter()
  {
    this.currentTab=this.userProvider.currentTab;
  }
}
