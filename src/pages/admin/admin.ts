import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../service/GlobalVar'

@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {
  accountList:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider) {
  }

  ionViewDidLoad() {
    this.accountList=JSON.parse(localStorage.getItem('Accounts'))||[];
  }
  delete(v:number)
  {
    var value=JSON.parse(localStorage.getItem('Accounts'))||[];
    var newArr=[];
    if(this.userProvider.adminName==value[v].username)
    {
      this.userProvider.checklogin = false;
      this.userProvider.closeform = false;
      this.userProvider.checkLogin = false; 
      this.userProvider.currentUsername='';
    }
    for(var i = 0; i<value.length;i++)
    {
      if(i!=v)
      {
        newArr.push(value[i]);
      }
    }
    localStorage.setItem('Accounts', JSON.stringify(newArr));
    this.userProvider.adminName=newArr[0].username;
    this.accountList=JSON.parse(localStorage.getItem('Accounts'))||[];
  }
}
