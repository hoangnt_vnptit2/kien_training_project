import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    console.log("home");
  }
  ngOnInit(){console.log("ngoninitHOME")};
  onNavToHome(){
    this.navCtrl.pop();
  }

  // ionViewWillEnter(){console.log("Chuẩn bị vào trang home")};
  // ionViewDidEnter(){console.log("Đã vào vào trang home")};
  // ionViewWillLeave(){console.log("Chuẩn bị rời trang home")};
  // ionViewDidLeave(){console.log("Đã rời trang home")};
  // ionViewDidLoad(){console.log("loaded home")}
}
