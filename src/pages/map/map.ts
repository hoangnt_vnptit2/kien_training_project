import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { UserProvider } from '../../service/GlobalVar'

declare var google: any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  @ViewChild('map') public mapRef: ElementRef;
  static information = [];
  // map: GoogleMap;
  markers = [];


  constructor(public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public userProvider: UserProvider) { }

  ionViewDidEnter() {
    this.DisplayMap();
  }

  DisplayMap() {
    var geocoder = new google.maps.Geocoder();
    const location = new google.maps.LatLng('10.7852113', '106.69324529999994');
    var options = {
      center: location,
      zoom: 10,
      title: 'VNPT IT2'
    }

    var map = new google.maps.Map(this.mapRef.nativeElement, options);

    var marker = new google.maps.Marker({
      position: location,
      map: map,
      title: "Hello World!"
    });
    var address:any;
    let self = this;
    geocoder.geocode({ 'latLng': location }, function (data, err) {
      address=data[4].formatted_address;
      let infoWindow = new google.maps.InfoWindow({
        content: data[4].formatted_address
      });
      infoWindow.open(map, marker);
      self.userProvider.infor.push({ 'longitude': location.lng(), 'latitude': location.lat(), 'address': address });
    });
    // geocoder.geocode({'address':'42, Phạm Ngọc Thạch, Quận 3'}, function (data, err) {
    //   // console.log("adress", data);
    //   console.log("adressLat", data[0].geometry.location.lat());
    //   console.log("adress", data[0].geometry.location.lng());
    //   console.log("err", err);
    // });

    
    google.maps.event.addListener(map, 'click', function (event) {
      marker.setMap(null);

      map.setCenter(event.latLng);
      map.setZoom(16);

      options = {
        center: event.latLng,
        zoom: 10,
        title: 'VNPT IT2'
      }

      self.userProvider.check = self.userProvider.check + 1;
      map.center = event.latLng;

      marker = new google.maps.Marker({
        position: event.latLng,
        map: map,
        title: "vvv!"
      })

      geocoder.geocode({ 'latLng': event.latLng }, function (data, err) {
        address = data[0].formatted_address;
        let infoWindow = new google.maps.InfoWindow({
          content: data[0].formatted_address
        });
        infoWindow.open(map, marker);
        self.userProvider.infor.push({ 'longitude': event.latLng.lng(), 'latitude': event.latLng.lat(), 'address': address });
      });
    });
  };
}
