import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../service/GlobalVar'
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider) {
    console.log('HistoryPage');
  }

}
