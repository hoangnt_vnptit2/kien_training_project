import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { DetailPage } from '../detail/detail';
import 'rxjs/add/operator/map';
import {UserProvider} from '../../service/GlobalVar'


@Component({
  selector: 'page-tab-business',
  templateUrl: 'tab-business.html',
})
export class TabBusinessPage {
  myInput: any = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public userProvider: UserProvider) {
  }


  ionViewDidEnter()
  {
    this.userProvider.category='business';
    this.userProvider.setAPI();
    this.userProvider.currentTab=0;
  }

  ionSelected()
  {
    console.log("chuyển tab business")
  }

  detail(i: number) {
    var data = this.userProvider.result[i];
    this.navCtrl.push(DetailPage, { dt: data });
  }
  onInput() {
    this.userProvider.result = this.userProvider.data.filter((item) => {
      return item.title.toLowerCase().includes(this.myInput.toLowerCase());
    });
  }
}
