import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UserProvider } from '../../service/GlobalVar';
// import { LocalStorageService } from '../../service/LocalStorageService'
// import { LocalStorageProvider } from '../../providers/local-storage/local-storage'

@Component({
  selector: 'page-login-logout',
  templateUrl: 'login-logout.html',
})
export class LoginLogoutPage {
  arr = [];
  username: string;
  password: string;
  flag = false;
  count = 0;
  loginfalse = false;
  intervalId = 0;
  message = '';
  seconds = 60;
  usernameErr = false;
  passwordErr = false;
  // loginErr='Username or Password incorrect';
  loginErr = false;
  userExists = false;

  clearTimer() { clearInterval(this.intervalId); }

  constructor(
    // public localStorageService: LocalStorageService,
    // public localStorageProvider: LocalStorageProvider,
    public alertController: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider

  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginLogoutPage', JSON.parse(localStorage.getItem('Accounts')) || []);
    var value = JSON.parse(localStorage.getItem('Accounts')) || [];
    for (var i = 0; i < value.length; i++) {
      if (value[i].status == 1) {
        this.userProvider.checklogin = true;
        this.userProvider.closeform = false;
        this.userProvider.checkLogin = true;
        this.userProvider.currentUsername = value[i].username;
      }
    }
    if (value.length >= 1) {
      this.userProvider.adminName = value[0].username;
    }
  }
  onLogin() {

    this.count += 1;
    var value = JSON.parse(localStorage.getItem('Accounts')) || [];
    for (var i = 0; i < value.length; i++) {
      if (this.username == value[i].username && this.password == value[i].password) {
        this.userProvider.image = value[i].image;
        this.userProvider.checklogin = true;
        this.userProvider.closeform = false;
        this.userProvider.checkLogin = true;
        this.count = 0;
        this.loginErr = false;
        this.passwordErr = false;
        this.usernameErr = false;
        this.userProvider.currentUsername = this.username;
        value[i].status = 1;
        if (value.length >= 1) {
          this.userProvider.adminName = value[0].username;
        }
        localStorage.setItem('Accounts', JSON.stringify(value));
        break;
      }
    }
    if (this.userProvider.checklogin == false && this.userProvider.closeform == false && this.userProvider.checkLogin == false) {
      if (this.count > 5) {
        this.countDown();
        this.seconds = 60;
        this.count = 0;
        this.loginErr = false;
      }
      else {
        this.loginErr = true;
      }
    }

  }

  countDown() {
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      if (this.seconds === 0) {
        this.userProvider.checklogin = false;
        this.userProvider.closeform = false;
        this.count = 0;
      } else {
        this.userProvider.checklogin = true;
        this.userProvider.closeform = true;
        if (this.seconds < 0) {
          this.userProvider.checklogin = false;
          this.userProvider.closeform = false;
        } // reset
        this.message = `Đăng nhập sai 5 lần, vui lòng thử lại sau: ${this.seconds}s`;
      }
    }, 1000);
  }

  onSignup() {
    var isNum = 0;
    var isLower = 0;
    var isUpper = 0;
    var isSymbol = 0;
    this.userExists = false;

    if (this.username == null || this.password == null) {
      console.log("Tên đăng nhập hoặc mật khẩu rỗng")
    }
    else {
      var usernameArr = this.username.split('');
      var passwordArr = this.password.split('');

      for (var i = 0; i < passwordArr.length; i++) {
        if (isNaN(Number(passwordArr[i].toString())) == false) {
          isNum += 1;
        }
        // Kiểm tra chữ hoa
        if (/[A-Z]/.test(passwordArr[i])) {
          isUpper += 1;
        }
        //Kiểm tra chữ thường
        if (/[a-z]/.test(passwordArr[i])) {
          isLower += 1;
        }
        if (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`]/.test(passwordArr[i])) {
          isSymbol += 1
        }
      }

      if (isNum > 0 && isUpper > 0 && isLower > 0 && isSymbol > 0 && usernameArr.length >= 8 && usernameArr.length <= 20) {
        var value = JSON.parse(localStorage.getItem('Accounts')) || [];
        for (var j = 0; j < value.length; j++) {
          if (this.username == value[j].username) {
            this.userExists = true;
            this.usernameErr = false;
          }
        }

        if (this.userExists == false) {
          value.push({
            username: this.username,
            password: this.password,
            status: 1,
            image: 'https://icon-library.net/images/default-user-icon/default-user-icon-8.jpg'
          })
          if (value.length == 1) {
            this.userProvider.adminName = value[0].username;
          }
          this.userProvider.checklogin = true;
          this.userProvider.closeform = false;
          console.log("đăng ký hợp lệ");
          localStorage.setItem('Accounts', JSON.stringify(value));

          console.log(JSON.parse(localStorage.getItem('Accounts')))
          this.usernameErr = false;
          this.passwordErr = false;
          this.userProvider.currentUsername = this.username;
          this.userProvider.checkLogin = true;
          this.userProvider.image = 'https://icon-library.net/images/default-user-icon/default-user-icon-8.jpg';
        }

        // this.localStorageService.storeOnLocalStorage(this.username, this.password);
        // this.localStorageProvider.storeOnLocalStorage(this.username, this.password);
      }
      else if ((isNum == 0 || isUpper == 0 || isLower == 0 || isSymbol == 0) && (usernameArr.length < 8 || usernameArr.length > 20)) {
        console.log("Tên đăng nhập và mật khẩu không hợp lệ")
        this.usernameErr = true;
        this.passwordErr = true;
      }
      else if (isNum > 0 && isUpper > 0 && isLower > 0 && isSymbol > 0 || (usernameArr.length < 8 || usernameArr.length > 20)) {
        console.log("tên đăng nhập từ 8 tới 20 ký tự")
        this.passwordErr = false;
        this.usernameErr = true;
      }
      else if (isNum == 0 || isUpper == 0 || isLower == 0 || isSymbol == 0 && (usernameArr.length > 8 && usernameArr.length < 20)) {
        console.log("Mật khẩu phải có chữ hoa, thường, số, ký tự đặc biệt kết hợp");
        this.usernameErr = false;
        this.passwordErr = true;
      }

    }
  }

  async onLogout() {
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'Are you sure you want to log out',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            var value = JSON.parse(localStorage.getItem('Accounts')) || [];
            for (var i = 0; i < value.length; i++) {
              if (this.userProvider.currentUsername == value[i].username) {
                value[i].status = 0;
                localStorage.setItem('Accounts', JSON.stringify(value));
                break;
              }
            }
            this.username = '';
            this.password = '';
            this.userProvider.currentUsername = '';
            this.userProvider.checklogin = false;
            this.userProvider.closeform = false;
            this.userProvider.checkLogin = false;

          }
        }
      ]
    });

    await alert.present();
  }
  openSignup() {
    this.userProvider.checklogin = false;
    this.userProvider.closeform = true;
  }
  back() {
    this.userProvider.checklogin = false;
    this.userProvider.closeform = false;
    this.userProvider.checkLogin = false;
  }

}
