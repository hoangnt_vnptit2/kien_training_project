import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { UserProvider } from '../../service/GlobalVar';

@Component({
  selector: 'page-set-avatar',
  templateUrl: 'set-avatar.html',
})
export class SetAvatarPage {
  myphoto = 'https://icon-library.net/images/default-user-icon/default-user-icon-8.jpg';
  checkPhotoChange = false;
  displayMess = false;
  // obj={id:"01"};

  constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public userProvider: UserProvider) {
  }

  ionViewDidLoad() {
    this.myphoto =  this.userProvider.image;
    // this.obj=Object.assign(this.obj,{key: "Phạm TRung Kiên"});
    // console.log(this.obj);
  }

  takePhoto() {
    this.displayMess = false;
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.checkPhotoChange = true;
    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    this.displayMess = false;
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.checkPhotoChange = true;
    }, (err) => {
      // Handle error
    });
  }

  saveImage() {
    var value = JSON.parse(localStorage.getItem('Accounts')) || [];
    for (var i = 0; i < value.length; i++) {
      if (value[i].username == this.userProvider.currentUsername) {
        value[i].image = this.myphoto;
        this.userProvider.image = this.myphoto;
        this.checkPhotoChange = false;
        this.displayMess = true;
        localStorage.setItem('Accounts', JSON.stringify(value));
      }
    }
  }

  cancelImage() {
    this.checkPhotoChange = false;
    this.myphoto = this.userProvider.image;
  }
}
