import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import {DetailPage} from '../detail/detail';
import {UserProvider} from '../../service/GlobalVar'

@Component({
  selector: 'page-tab-science',
  templateUrl: 'tab-science.html',
})
export class TabSciencePage {

  myInput : any="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public userProvider: UserProvider) {
  }
  ionViewDidEnter()
  {
    this.userProvider.category='science';
    this.userProvider.setAPI();
    this.userProvider.currentTab=4;
  }


  detail(i: number) {
    var data = this.userProvider.result[i];
    this.navCtrl.push(DetailPage, { dt: data });
  }
  onInput() {
    this.userProvider.result = this.userProvider.data.filter((item) => {
      return item.title.toLowerCase().includes(this.myInput.toLowerCase());
    });
  }
}
