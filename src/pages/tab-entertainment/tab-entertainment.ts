import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import {DetailPage} from '../detail/detail';
import {UserProvider} from '../../service/GlobalVar'

@Component({
  selector: 'page-tab-entertainment',
  templateUrl: 'tab-entertainment.html',
})
export class TabEntertainmentPage {

  myInput : any="";

  category='entertainment';
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public userProvider: UserProvider) {
  }

  ionViewDidEnter()
  {
    this.userProvider.category='entertainment';
    this.userProvider.setAPI();
    this.userProvider.currentTab=1;
  }
  detail(i:number){
    var data = this.userProvider.result[i];
    this.navCtrl.push(DetailPage, {dt:data});
  }
  onInput()
  {
    this.userProvider.result=this.userProvider.data.filter((item) => {
      return item.title.toLowerCase().includes(this.myInput.toLowerCase());
  });  
  }
}



