import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const STORAGE_KEY = 'Accounts';

@Injectable()
export class LocalStorageProvider {

  constructor(public storage: Storage) {
    console.log('Hello LocalStorageProvider Provider');
  }
  public storeOnLocalStorage(username : string, password : string): void {
          
    // get array of tasks from local storage
    var currentTodoList:any; 
    this.storage.get(STORAGE_KEY).then(rs =>{
      currentTodoList=rs
    }) || [];
    // push new task to array
    currentTodoList.push({
        username: username,
        password: password
    });
    // insert updated array to local storage
    this.storage.set(STORAGE_KEY, currentTodoList);
    console.log(this.storage.get(STORAGE_KEY) || 'LocaL storage is empty');
}

//  public checkAccount(username : string, password : string): boolean{
//      for(var i = 0;i<this.storage.get(STORAGE_KEY).length; i++)
//      {
//          if(this.storage.get(STORAGE_KEY)[i].username==username && this.storage.get(STORAGE_KEY)[i].password==password)
//          {
//              return true;
//              break;
//          }
//      }
//      return false;
//  }
}
