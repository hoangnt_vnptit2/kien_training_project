import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClient } from '@angular/common/http';

import {Test1Page} from '../pages/Test1/test1';
import {MapPage} from '../pages/map/map'
import {MapTestPage} from '../pages/map-test/map-test'
import {HistoryPage} from '../pages/history/history';
import {UserProvider} from '../service/GlobalVar';
import {LoginLogoutPage} from '../pages/login-logout/login-logout';
import {AdminPage} from '../pages/admin/admin';
import { SetAvatarPage } from '../pages/set-avatar/set-avatar';
import {TrangChuPage} from '../pages/trang-chu/trang-chu'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = Test1Page;
  pages: Array<{title: string, component: any}>;
  ct = 'us';
  countries = [
    { name: 'us' }, { name: 'ae' }, { name: 'ar' }, { name: 'at' }, { name: 'au' }, { name: 'be' }, { name: 'bg' }, { name: 'kr' }, { name: 'jp' },
    { name: 'br' }, { name: 'ca' }, { name: 'ch' }, { name: 'cn' }, { name: 'co' }, { name: 'cu' }, { name: 'cz' }, { name: 'de' }, { name: 'eg' },
    { name: 'fr' }, { name: 'gb' }, { name: 'gr' }, { name: 'hk' }, { name: 'hu' }, { name: 'id' }, { name: 'ie' }, { name: 'il' }, { name: 'in' }, { name: 've' },
  ];

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public http: HttpClient,
    public menu: MenuController, 
    public userProvider: UserProvider,
    public events: Events) 
  {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    })
    // set our app's pages
    this.pages = [
      { title: 'Home', component: Test1Page},
      { title: 'Map', component: MapPage },
      // { title: 'MapTest', component: MapTestPage },
      { title: 'History', component: HistoryPage },
      { title: 'Account', component: LoginLogoutPage },
      { title: 'Admin', component: AdminPage },
      { title: 'Trang chủ', component: TrangChuPage }


    ]

    var value = JSON.parse(localStorage.getItem('Accounts')) || [];
    for (var i = 0; i < value.length; i++) {
      if (value[i].status == 1) {
        this.userProvider.currentUsername = value[i].username;
        this.userProvider.image = value[i].image;
      }
    }
  };

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
    // this.check = MapPage.information.length;
  }

  changeCountry() {
    this.menu.close();
    this.userProvider.country=this.ct;
    this.userProvider.setAPI();
  }
  toSetAvatar()
  {
    this.menu.close();
    this.nav.push(SetAvatarPage);
  }
}

