import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { StorageServiceModule } from 'ngx-webstorage-service';
import {LocalStorageService} from '../service/LocalStorageService';
import { IonicStorageModule } from '@ionic/storage';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { Camera } from '@ionic-native/camera';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Test1Page } from '../pages/Test1/test1';
import { TabBusinessPage } from '../pages/tab-business/tab-business';
import { TabEntertainmentPage } from '../pages/tab-entertainment/tab-entertainment';
import { TabGeneralPage } from '../pages/tab-general/tab-general';
import { TabHealthPage } from '../pages/tab-health/tab-health';
import { TabSciencePage } from '../pages/tab-science/tab-science';
import { TabSportsPage } from '../pages/tab-sports/tab-sports';
import { DetailPage } from '../pages/detail/detail';
import { TabsPage } from '../pages/tabs/tabs';
import { MapPage } from '../pages/map/map';
import { TrangChuPage } from '../pages/trang-chu/trang-chu';
import {GoogleMaps} from '@ionic-native/google-maps';
import {MapTestPage} from '../pages/map-test/map-test'
import { Geolocation } from '@ionic-native/geolocation';
import { HistoryPage} from '../pages/history/history';
import {UserProvider} from '../service/GlobalVar';
import {LoginLogoutPage} from '../pages/login-logout/login-logout';
import {AdminPage} from '../pages/admin/admin';
import { SetAvatarPage } from '../pages/set-avatar/set-avatar'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Test1Page,
    TabBusinessPage,
    TabEntertainmentPage,
    TabGeneralPage,
    TabHealthPage,
    TabSciencePage,
    TabSportsPage,
    DetailPage,
    TabsPage,
    MapPage,
    TrangChuPage,
    MapTestPage,
    HistoryPage,
    LoginLogoutPage,
    AdminPage,
    SetAvatarPage

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StorageServiceModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAHd6e8tKgACvt5P_Qjbn5wsxIMSv_BPRM'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Test1Page,
    TabBusinessPage,
    TabEntertainmentPage,
    TabGeneralPage,
    TabHealthPage,
    TabSciencePage,
    TabSportsPage,
    DetailPage,
    TabsPage,
    MapPage,
    TrangChuPage,
    MapTestPage,
    HistoryPage,
    LoginLogoutPage,
    AdminPage,
    SetAvatarPage
  ],
  providers: [
    GoogleMaps,
    Geolocation,
    StatusBar,
    UserProvider,
    LocalStorageService,
    Camera,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LocalStorageProvider
  ]
})
export class AppModule { }
