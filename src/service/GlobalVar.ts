import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserProvider {
  check = 0;
  infor = [];
  country = 'us';
  category = 'business';
  dt: any;
  data: any;
  result: any;
  adminName = 'KienPham';
  currentUsername = '';
  checkLogin = false;
  checklogin = false;
  closeform = false;
  currentTab: number = 0;
  hotnew:any;
  image = 'https://icon-library.net/images/default-user-icon/default-user-icon-8.jpg';

  constructor(public http: HttpClient) {

  }
  setNumber(v: number) {
    this.check = v;
  }

  setAPI() {
    var value = JSON.parse(localStorage.getItem('DataNew')) || [];

    this.http.get(`https://newsapi.org/v2/top-headlines?country=${this.country}&category=${this.category}&apiKey=06849ffbcb1542fb9e8708e597c28a67`)
      .subscribe(data => {
        this.dt = data;

        for (var i = 0; i < this.dt.articles.length; i++) {
          if (value.length > 0) {
            var flag = 0;

            for (var j = 0; j < value.length; j++) {
              if (this.dt.articles[i].title == value[j].title) {
                flag += 1;
              }
            }

            if (flag == 0) {
              this.dt.articles[i] = Object.assign(this.dt.articles[i], { dislike: 0, like: 0, comment: 0, id: value.length, category: this.category });
              value.push(this.dt.articles[i]);
            }
          }
          else {
            this.dt.articles[i] = Object.assign(this.dt.articles[i], { dislike: 0, like: 0, comment: 0, id: value.length, category: this.category });
            value.push(this.dt.articles[i]);
          }
        }

        localStorage.setItem('DataNew', JSON.stringify(value));

        if (this.category == 'business') {
          var businessDT = [];

          for (var business = value.length - 1; business >= 0; business--) {
            if (value[business].category == 'business') {
              businessDT.push(value[business]);
            }
          }
          this.result = businessDT;
          this.data = businessDT;
        }

        if (this.category == 'entertainment') {
          var entertainmentDT = [];

          for (var a = value.length - 1; a >= 0; a--) {
            if (value[a].category == 'entertainment') {
              entertainmentDT.push(value[a]);
            }
          }
          this.result = entertainmentDT;
          this.data = entertainmentDT;
        }

        if (this.category == 'general') {
          var generalDT = [];

          for (var a = value.length - 1; a >= 0; a--) {
            if (value[a].category == 'general') {
              generalDT.push(value[a]);
            }
          }
          this.result = generalDT;
          this.data = generalDT;
        }

        if (this.category == 'health') {
          var healthDT = [];

          for (var a = value.length - 1; a >= 0; a--) {
            if (value[a].category == 'health') {
              healthDT.push(value[a]);
            }
          }
          this.result = healthDT;
          this.data = healthDT;
        }

        if (this.category == 'science') {
          var scienceDT = [];

          for (var a = value.length - 1; a >= 0; a--) {
            if (value[a].category == 'science') {
              scienceDT.push(value[a]);
            }
          }
          this.result = scienceDT;
          this.data = scienceDT;
        }

        if (this.category == 'sports') {
          var sportsDT = [];

          for (var a = value.length - 1; a >= 0; a--) {
            if (value[a].category == 'sports') {
              sportsDT.push(value[a]);
            }
          }
          this.result = sportsDT;
          this.data = sportsDT;
        }

        console.log("data", this.data)
      })
  }

  hotNews() {
    var value = JSON.parse(localStorage.getItem('DataNew')) || [];
    var max = value[0];

    for (var i = 0; i < value.length; i++) {
      var hot = value[i].like + value[i].disslike + value[i].comment;

      for (var j = i + 1; j < value.length; j++) {
        var hot1 = value[j].like + value[j].disslike + value[j].comment;

        if (hot1 > hot) {
          var flag = value[i];
          value[i] = value[j];
          value[j] = flag;
        }
      }
    }
    
  }

}